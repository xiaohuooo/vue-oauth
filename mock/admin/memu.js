
const list = [
    {
        name: '系统管理',
        id: '0',
        path: '/admin',
        icon: 'Monitor',
        redirect: '/admin/memu',
        children: [
            {
                name: '菜单管理',
                path: '/admin/memu',
                component: '/admin/memu/memu',
                id: '1',
                icon: 'Operation'
            },
            {
                name: '用户管理',
                path: '/admin/user',
                component: '/admin/user/index',
                id: '2',
                icon: 'UserFilled'
            }
        ]
    }
];
const list1 = [
    {
        name: '系统管理1',
        id: '0',
        path: '/admin',
        icon: 'Monitor',
        redirect: '/admin/memu',
        children: [
            {
                name: '菜单管理1',
                path: '/admin/memu',
                component: '/admin/memu/memu',
                id: '1',
                icon: 'Operation'
            },
        ]
    }
];
export default [
    {
        url: '/admin/memu',
        method: 'post',
        response: (params) => {
            let data = ''
            params.headers.authorization.includes('admin') ? data = list : data = list1
            return {
                code: 200,
                message: 'success',
                data
            }
        }
    }
];
