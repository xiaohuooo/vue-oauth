import Mock from 'mockjs';

export default [
    {
        url: '/admin/login',
        method: 'post',
        response: (params) => {
            return {
                code: 200,
                message: 'success',
                data: Mock.mock({
                    'token': params.body.userName + 'xxxxxxxxxxxxxxxxxxxxx'
                })
            }
        }
    }
]
